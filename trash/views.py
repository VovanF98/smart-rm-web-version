from .models import *
from django.shortcuts import render, get_object_or_404, redirect
from .forms import *
from Removing_operations.SrmF98 import SrmHandler
from Trash_operations.Trash import Trash
from Config.Config import Config_file
from Extra_operations import Extra_operations
from .file_system import *
import shutil
import os
from Checker.Checker import make_dir
from django.views.decorators.csrf import csrf_exempt
from django.http.response import JsonResponse
import file_system
import Checker.Checker as Checker


@csrf_exempt
def trash_list(request):
    trashes = Web_Trash.objects.all()
    for trash in trashes:
        trash.current_size = Extra_operations.dir_size(os.path.join(trash.path_to_trash, "files"))
    return render(request, 'trash/trash_list.html', {'posts': trashes})


@csrf_exempt
def trash_detail(request, pk):
    trash = get_object_or_404(Web_Trash, pk=pk)
    trash.current_size = Extra_operations.dir_size(os.path.join(trash.path_to_trash, "files"))
    list_of_files = dir_list(os.path.join(trash.path_to_trash, 'info'), os.path.join(trash.path_to_trash, 'files'))
    return render(request, 'trash/trash_detail.html', {'PK': pk, 'form': trash, 'files': list_of_files})


@csrf_exempt
def add_regular_task(request):
    data = dict(request.POST)
    if not data:
        form = RegTaskForm()
        trashes = Web_Trash.objects.all()
        return render(request, "trash/regular_delete.html", {'form': form, 'trashes': trashes})
    cur_id = str(data["current_trash"][0]).encode('ascii', 'ignore')
    cur_trash = Web_Trash.objects.filter(id=int(cur_id))
    reg_task_instance = Regular_Task(initial_folder=data["initial_folder"][0].encode('ascii', 'ignore'),
                                     regular_expression=data["regular_expression"][0].encode('ascii', 'ignore'),
                                     current_trash=cur_trash.first())
    reg_task_instance.save()
    return redirect(list_of_tasks)


@csrf_exempt
def delete_task(request,pk=0):
    data = dict(request.POST)
    cur_id = int(str(data["task_id"][0]).encode("ascii", "ignore"))
    Task.objects.filter(id=cur_id).delete()
    return redirect(list_of_tasks)


@csrf_exempt
def delete_regular_task(request,pk=0):
    data = dict(request.POST)
    cur_id = int(str(data["task_id"][0]).encode("ascii", "ignore"))
    reg_task = Regular_Task.objects.filter(id=cur_id).first()
    Regular_Task.objects.filter(id=cur_id).delete()
    return redirect(list_of_tasks)


@csrf_exempt
def execute_task(request, pk=0):
    data = dict(request.POST)
    final_list = file_system.prepare_list_of_files(data)
    trash_instance = Web_Trash.objects.filter(name=data["trash"][0]).first()
    config_instance = Config_file(os.path.join(trash_instance.path_to_trash, trash_instance.name + '.cfg'))
    config_data = config_instance.get_config_info()
    if trash_instance.dry_run == 'True':
        dry_run = True
    else:
        dry_run = False
    srm_instance = SrmHandler(config_data, data["trash"][0], dry_run, False, False)
    trash_instance = Trash(config_data, data["trash"][0], dry_run, False, False)
    cur_id = int(str(data["task_id"][0]).encode("ascii", "ignore"))
    task = Task.objects.filter(id=cur_id).first()
    completed_task = Completed_task(current_trash=task.current_trash, files=task.files, type_of_operation=task.type_of_operation,
                                    status="complete")
    if data["operation"][0] == "Remove to trash":
        srm_instance.multi_delete_to_trash(final_list, data, config_data)
        if not succes_removing_operation(final_list, dry_run):
            completed_task.status = "Error"
    if data["operation"][0] == "Remove from system":
        trash_instance.multi_delete_from_trash(final_list, data, config_data)
        if not success_clearing_operation(trash_instance.path_to_trash, final_list, dry_run):
            completed_task.status = "Error"
    if data["operation"][0] == "Recover from trash":
        trash_instance.multi_recover_from_trash(final_list, data, config_data)
        if not success_restoring_operation(trash_instance.path_to_trash, final_list, dry_run):
            completed_task.status = "Error"
    task.delete()
    completed_task.save()
    return redirect(list_of_tasks)


@csrf_exempt
def execute_regular_task(request, pk=0):
    data = dict(request.POST)
    trash_instance = Web_Trash.objects.filter(name=data["trash"][0]).first()
    config_instance = Config_file(os.path.join(trash_instance.path_to_trash, trash_instance.name + '.cfg'))
    config_data = config_instance.get_config_info()
    if trash_instance.dry_run == 'True':
        dry_run = True
    else:
        dry_run = False
    srm_instance = SrmHandler(config_data, data["trash"][0], dry_run, False, False)
    cur_id = int(str(data["task_id"][0]).encode("ascii", "ignore"))
    reg_task = Regular_Task.objects.filter(id=cur_id).first()
    final_list = srm_instance.remove_regexp(data["initial_folder"][0], data["regular_expression"][0])
    completed_task = Completed_task(current_trash=reg_task.current_trash, files=final_list,
                                    type_of_operation="Remove to trash",
                                    status="complete")
    srm_instance.multi_delete_to_trash(final_list, data, config_data)
    if not succes_removing_operation(final_list, dry_run):
        completed_task.status = 'Error'
    completed_task.save()
    delete_regular_task(request)
    return redirect(list_of_tasks)


@csrf_exempt
def list_of_tasks(request, pk=0):
    tasks = Task.objects.all()
    regular_tasks = Regular_Task.objects.all()
    return render(request, 'trash/list_of_tasks.html', locals())


@csrf_exempt
def remove_from_trash(request, pk):
    data = dict(request.POST)
    trash = Web_Trash.objects.filter(id=pk).first()
    task = Task(current_trash=trash, files=[data_of_file.encode('ascii', 'ignore') for data_of_file in data['hashes[]']],
                type_of_operation='Remove from system')
    task.save()
    return redirect(list_of_tasks)


@csrf_exempt
def recover_from_trash(request, pk):
    data = dict(request.POST)
    trash = Web_Trash.objects.filter(id=pk).first()
    task = Task(current_trash=trash, files=[data_of_file.encode('ascii', 'ignore') for data_of_file in data['hashes[]']],
                type_of_operation='Recover from trash')
    task.save()
    return redirect(list_of_tasks)


@csrf_exempt
def add_task(request):
    data = dict(request.POST)
    current_trash = Web_Trash.objects.get(name=data['trash'][0])
    task_instance = Task(current_trash=current_trash,
                         files=[data_of_file.encode('ascii', 'ignore') for data_of_file in data['files[]']],
                         type_of_operation='Remove to trash')
    task_instance.save()
    return redirect(list_of_tasks)


@csrf_exempt
def trash_new(request):
    if request.method == "POST":
        form = TrashForm(request.POST)
        if form.is_valid():
            data = dict(form.data)
            name = data["name"][0].encode('ascii', 'ignore')
            if os.path.exists(Checker.from_list_to_string(data["path_to_trash"])):
                data["path_to_trash"] = Checker.from_list_to_string(data["path_to_trash"])
            else:
                data["path_to_trash"] = os.path.join(os.path.expanduser("~"), "Desktop")
            data["path_to_trash"] = os.path.join(data["path_to_trash"], name)
            make_dir(data["path_to_trash"])
            trash = Trash(data, name, False, False, False)
            conf_file = Config_file(trash.path_to_cfg)
            conf_file.make_config_file(os.path.join(trash.path_to_trash, name), data["restoring_politic"][0],
                                       data["maximum_size_of_trash"][0], data["number_of_objects_to_auto_delete"][0],
                                       data["deleting_politic"][0],
                                       cur_size=int(Extra_operations.dir_size(trash.files)))
            form.path_to_trash = os.path.join(trash.path_to_trash, name)
            all_trashes = Web_Trash.objects.all()
            for item in all_trashes:
                if item.path_to_trash == trash.path_to_trash:
                    return redirect(trash_list)
            post = form.save()
            post.path_to_trash = trash.path_to_trash
            post.current_size = int(Extra_operations.dir_size(trash.files))
            post.save()
            Wtrash = get_object_or_404(Web_Trash, pk=post.pk)
            return render(request, 'trash/trash_detail.html', {'PK': post.pk, 'form': Wtrash})
    else:
        form = TrashForm()
    return render(request, 'trash/trash_new.html', {'form': form})


@csrf_exempt
def trash_edit(request, pk):
    trash = get_object_or_404(Web_Trash, pk=pk)
    if request.method == "POST":
        form = TrashForm(request.POST, instance=trash)
        if form.is_valid():
            trash = Web_Trash.objects.filter(id=pk).first()
            data = form.data
            os.remove(os.path.join(trash.path_to_trash, trash.name + '.cfg'))
            if not os.path.exists(data["path_to_trash"]):
                path_to_trash = os.path.expanduser("~/Desktop")
            else:
                if trash.path_to_trash != data["path_to_trash"]:
                    path_to_trash = data["path_to_trash"]
                else:
                    path_to_trash = os.path.dirname(data["path_to_trash"])
            conf_file = Config_file(os.path.join(trash.path_to_trash, data["name"] + '.cfg'))
            conf_file.make_config_file(path_to_trash=os.path.join(path_to_trash, data["name"]),
                                       cleaning_politic=data["deleting_politic"],
                                       size_of_trash=data["maximum_size_of_trash"],
                                       am_to_delete=data["number_of_objects_to_auto_delete"],
                                       restoring_politic=data["restoring_politic"],
                                       cur_size=int(Extra_operations.dir_size(os.path.join(os.path.join(trash.path_to_trash),'files'))))

            past_path = trash.path_to_trash
            post = form.save()
            if os.path.exists(data["path_to_trash"]) and os.path.isdir(data["path_to_trash"]):
                if data["path_to_trash"] != trash.path_to_trash:
                    make_dir(os.path.join(data["path_to_trash"], data["name"]))
                    trash.path_to_trash = os.path.join(data["path_to_trash"], data["name"])
                    os.rename(past_path, trash.path_to_trash)
                    post.path_to_trash = trash.path_to_trash
                else:
                    make_dir(os.path.join(os.path.dirname(data["path_to_trash"]), data["name"]))
                    print data["path_to_trash"]
                    print os.path.join(os.path.dirname(data["path_to_trash"]), data["name"])
                    os.rename(data["path_to_trash"], os.path.join(os.path.dirname(data["path_to_trash"]), data["name"]))
                    trash.path_to_trash = os.path.join(os.path.dirname(data["path_to_trash"]), data["name"])
            else:
                make_dir(os.path.join(os.path.expanduser("~/Desktop"), data["name"]))
                os.rename(trash.path_to_trash, os.path.join(os.path.expanduser("~/Desktop"), data["name"]))
                trash.path_to_trash = os.path.join(os.path.expanduser("~/Desktop"), data["name"])
                post.path_to_trash = os.path.join(os.path.expanduser("~/Desktop"), data["name"])
            post.save()
            past_path = trash.path_to_trash
            trash = get_object_or_404(Web_Trash, pk=post.pk)
            trash.current_size = Extra_operations.dir_size(os.path.join(past_path, "files"))
            trash.path_to_trash = past_path
            trash.save()
            list_of_files = dir_list(os.path.join(past_path, 'info'), os.path.join(past_path, "files"))
            return render(request, 'trash/trash_detail.html', {'PK': post.pk, 'form': trash, 'files': list_of_files})
    else:
        form = TrashForm(instance=trash)
    return render(request, 'trash/trash_edit.html', {'PK': pk, 'form': form})


@csrf_exempt
def trash_delete(request, pk):
    trash = Web_Trash.objects.filter(id=pk).first()
    if os.path.exists(trash.path_to_trash):
        shutil.rmtree(trash.path_to_trash)
    trash.delete()
    trashes = Web_Trash.objects.all()
    return render(request, 'trash/trash_list.html', {'posts': trashes})


@csrf_exempt
def delete_files(request):
    names = Web_Trash.objects.all()
    return render(request, 'trash/delete_files.html', {'names': names})


@csrf_exempt
def get_info_for_file_system(request):
    current_id = request.GET['id']
    if current_id == '#':
        home = os.path.expanduser('~')
        return JsonResponse(get_info(home), safe=False)
    else:
        return JsonResponse(get_info(current_id), safe=False)


@csrf_exempt
def list_completed_tasks(request):
    completed_tasks = Completed_task.objects.all()
    return render(request, "trash/list_of_completed_tasks.html", locals())


@csrf_exempt
def clear_completed_tasks(request):
    Completed_task.objects.all().delete()
    return redirect(list_completed_tasks)



