from django import forms
from .models import *


class TrashForm(forms.ModelForm):
    class Meta:
        model = Web_Trash
        fields = ('name', 'current_size', 'path_to_trash', 'maximum_size_of_trash',
                  'number_of_objects_to_auto_delete', 'deleting_politic', 'restoring_politic', 'dry_run')


class RegTaskForm(forms.ModelForm):
    class Meta:
        model = Regular_Task
        exclude = [""]
