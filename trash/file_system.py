import os


def get_info(current_path):
    if os.path.exists(current_path):
        files = os.listdir(current_path)
        list_of_files = []
        for each_file in files:
            path_of_file = os.path.join(current_path, each_file)
            if os.path.isdir(path_of_file):
                cur_file = {"id": path_of_file, "text": each_file,
                            "children": os.path.isdir(path_of_file)}
            else:
                cur_file = {"id": path_of_file, "text": each_file,
                            "icon": "glyphicon glyphicon-file"}
            list_of_files.append(cur_file)
        return list_of_files
    else:
        raise BaseException


def dir_size(path):
    total_size = 0
    if not os.path.isdir(path):
        return os.path.getsize(path)
    for dirpath, dirnames, filenames in os.walk(path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)
    return total_size


def dir_list(path, path_to_files):
    total_list =[]
    if not os.path.isdir(path):
        item = {'name': os.path.basename(path), 'path': path, 'size': dir_size(path)}
        if not item in total_list and os.path.basename(path) != 'info' and os.path.basename(path) != 'files':
            total_list.append({'name': os.path.basename(path), 'path': path, 'size':  dir_size(os.path.join(path_to_files, os.path.basename(item)))})
    for dirpath, dirnames, filenames in os.walk(path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            first = {'name': os.path.basename(fp), 'path': fp, 'size': dir_size(os.path.join(path_to_files, os.path.basename(fp)))}
            second = {'name': os.path.basename(dirpath), 'path': dirpath, 'size': dir_size(dirpath)}
            if not first in total_list and os.path.basename(fp) != 'info' and os.path.basename(fp) != 'files':
                total_list.append({'name': os.path.basename(fp), 'path': fp, 'size':  dir_size(os.path.join(path_to_files, os.path.basename(fp)))})
            basename = os.path.basename(dirpath)
            if not second in total_list and basename != 'info' and basename != 'files':
                total_list.append({'name': basename, 'path': dirpath, 'size':  dir_size(os.path.join(path_to_files, basename))})
    print total_list
    return total_list


def prepare_list_of_files(data):
    files = data["files"][0].encode('ascii', 'ignore')
    new_files = []
    bad_symbol = ';'
    cnt = 0
    s = ""
    files = "  " + files
    for name in files[1:-1]:
        if name == bad_symbol:
            cnt += 1
            if cnt % 2 == 0:
                new_files.append(s)
                s = ""
            else:
                s += name
        else:
            s += name
    final_list = []
    for name in new_files:
        final_list.append(name[7:-4])
    return final_list


def succes_removing_operation(list_of_files, dry_run):
    if dry_run:
        return True
    for name in list_of_files:
        if os.path.exists(name):
            return False
    return True


def success_clearing_operation(path_to_trash, list_of_files, dry_run):
    if dry_run:
        return True
    return success_restoring_operation(path_to_trash, list_of_files, dry_run)


def success_restoring_operation(path_to_trash, list_of_files, dry_run):
    if dry_run:
        return True
    for name in list_of_files:
        for item in os.listdir(os.path.join(path_to_trash, 'info')):
            if item == os.path.basename(name):
                return False
        for item in os.listdir(os.path.join(path_to_trash, 'files')):
            if item == os.path.basename(name):
                return False
    return True


if __name__ == '__main__':
    pass
