from django.db import models


class Web_Trash(models.Model):

    name = models.CharField(max_length=200)
    current_size = models.IntegerField()
    path_to_trash = models.CharField(max_length=100)
    maximum_size_of_trash = models.IntegerField()
    number_of_objects_to_auto_delete = models.IntegerField()
    by_size = 'by_size'
    by_date = 'by_date'
    DEL_CHOICES = (
        (by_size, 'by_size'),
        (by_date, 'by_date')
    )
    deleting_politic = models.CharField(max_length=100, choices=DEL_CHOICES, default=by_size)
    collision = 'collision'
    substitute = 'substitute'
    REST_CHOICES = (
        (substitute, 'substitute'),
        (collision, 'collision')
    )
    restoring_politic = models.CharField(max_length=100, choices=REST_CHOICES, default=collision)
    true = 'True'
    false = 'False'
    DRY_RUN = (
        (true, 'True'),
        (false, 'False')
    )
    dry_run = models.CharField(max_length=100, choices=DRY_RUN, default=false)

    def __str__(self):
        return self.name


class Completed_task(models.Model):
    current_trash = models.ForeignKey('Web_Trash', on_delete=models.CASCADE)
    files = models.TextField()
    type_of_operation = models.CharField(max_length=100)
    status = models.CharField(max_length=100)


class Task(models.Model):
    current_trash = models.ForeignKey('Web_Trash', on_delete=models.CASCADE)
    files = models.TextField()
    type_of_operation = models.CharField(max_length=100)


class Regular_Task(models.Model):
    current_trash = models.ForeignKey('Web_Trash', on_delete=models.CASCADE)
    regular_expression = models.CharField(max_length=200)
    initial_folder = models.CharField(max_length=200)

