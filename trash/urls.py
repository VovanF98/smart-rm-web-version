from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^list_completed_tasks', views.list_completed_tasks, name='list_completed_tasks'),
    url(r'^clear_completed_tasks', views.clear_completed_tasks, name='clear_completed_tasks'),
    url(r'^trash/(?P<pk>[0-9]+)/detail/list_of_tasks$', views.list_of_tasks, name='list_of_tasks'),
    url(r'^trash/(?P<pk>[0-9]+)/detail/$', views.trash_detail, name='trash_detail'),
    url(r'^trash/(?P<pk>[0-9]+)/detail/execute_task$', views.execute_task, name='execute_task'),
    url(r'^trash/(?P<pk>[0-9]+)/detail/execute_regular_task$', views.execute_regular_task, name='execute_regular_task'),
    url(r'^trash/(?P<pk>[0-9]+)/detail/delete_regular_task$', views.delete_regular_task, name='delete_regular_task'),
    url(r'^trash/(?P<pk>[0-9]+)/detail/delete_task$', views.delete_task, name='delete_task'),
    url(r'^trash/new/$', views.trash_new, name='trash_new'),
    url(r'^trash/(?P<pk>[0-9]+)/edit/$', views.trash_edit, name='trash_edit'),
    url(r'^delete_files', views.delete_files, name='delete_files'),
    url(r'get_info', views.get_info_for_file_system, name='get_info'),
    url(r'^trash/(?P<pk>[0-9]+)/delete/$', views.trash_delete, name='trash_delete'),
    url(r'^list_of_tasks', views.list_of_tasks, name='list_of_tasks'),
    url(r'^trash_list$', views.trash_list, name='trash_list'),
    url(r'^add_task', views.add_task, name='add_task'),
    url(r'^add_regular_task', views.add_regular_task, name='add_regular_task'),
    url(r'^delete_task', views.delete_task, name='delete_task'),
    url(r'^delete_regular_task', views.delete_regular_task, name='delete_regular_task'),
    url(r'^execute_task', views.execute_task, name='execute_task'),
    url(r'^execute_regular_task', views.execute_regular_task, name='execute_regular_task'),
    url(r'^trash/(?P<pk>[0-9]+)/detail/remove_from_trash', views.remove_from_trash, name='remove_from_trash'),
    url(r'^trash/(?P<pk>[0-9]+)/detail/recover_from_trash', views.recover_from_trash, name='recover_from_trash'),
    url(r'^', views.trash_list, name='home'),
]
